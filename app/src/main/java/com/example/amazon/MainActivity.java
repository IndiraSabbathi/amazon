package com.example.amazon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView ssnText = (TextView) findViewById(R.id.ssn);
        ssnText.setVisibility(View.GONE);
        TextView addressText = (TextView) findViewById(R.id.address);
        addressText.setVisibility(View.GONE);
        TextView nameText = (TextView) findViewById(R.id.name);
        nameText.setVisibility(View.GONE);

        Intent intent = this.getIntent();
        if(intent != null) {
            if(intent.getData() != null) {
                String address = intent.getData().getQueryParameter("address");
                String name = intent.getData().getQueryParameter("name");
                String ssn = intent.getData().getQueryParameter("ssn");
                Button button = (Button) findViewById(R.id.button1);
                //button.setText("Age: " + "age");
               // textView.setText("Your Social Security Number : "+ssn);
                nameText.setText("Name: " +name);
                nameText.setVisibility(View.VISIBLE);
                addressText.setText("Address: "+address);
                addressText.setVisibility(View.VISIBLE);
                ssnText.setText("SSN: "+ssn);
                ssnText.setVisibility(View.VISIBLE);
               // button.setVisibility(View.GONE);
            }
        }

       // TextView textView = (TextView)findViewById(R.id.ssn);
       // textView.setVisibility(View.GONE);
    }

    public void openBrowser(View view){
        //post request to the service
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://192.168.0.4:8080/mastercard/op/oauth?isAge>25&target_url=app://amazon.com/checkout?checkout=12"));
        startActivity(browserIntent);
    }

    public void submit(View view) throws JSONException {
        JSONObject jsonParam = new JSONObject();
        jsonParam.put("origin", "Amazon");
        jsonParam.put("targetRedirectUrl", "app://amazon.com/checkout?checkout=12");
        jsonParam.put("dataFields", "");
        final String body = jsonParam.toString();
        String url = "http://192.168.0.4:8080/tp/ageValidation/post";
       // String url = "http://192.168.0.4:8100/auth";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("mcid://MCID/verifym?mcid=1234&tpname=amazon"));
                startActivity(browserIntent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try{
                    return body == null? null : body.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);
    }

    public void openUserConcert(View view){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://192.168.0.4:8080/appSelection.html"));
        startActivity(browserIntent);
    }



}